# Prism Launcher offline

После некоторых событий с PolyMc, последним стало пользоваться нестабильно. Поэтому сразу появился "fork", который называется [Prism Launcher](https://github.com/PrismLauncher/PrismLauncher).

Идея патча, который позволяет включить оффлайн режим сразу, взята [отсюда](https://github.com/iamtakingithard/PolyMC-with-patches).

# Скачать

Уже скомпилированные версии доступны [здесь](https://gitlab.com/The220th/prism-launcher-offline/-/releases).

# Компиляция

## Arch Linux

``` bash
> yay
> yay cmake
> yay extra-cmake-modules
> yay qt6-tools
> yay qt6-5compat

> yay jdk-openjdk

> git clone --recursive https://github.com/PrismLauncher/PrismLauncher.git
> cd PrismLauncher
> wget https://gitlab.com/The220th/prism-launcher-offline/-/raw/main/0001-Remove-DRM.patch -O 0001-Remove-DRM.patch
> patch -p1 < "0001-Remove-DRM.patch"

> cmake -S . -B build -DCMAKE_INSTALL_PREFIX=install -DLauncher_QT_VERSION_MAJOR="6"
> cmake --build build -j$(nproc)
> cmake --install build
> cmake --install build --component portable
```

В папке `./install` появится портативный `Prism Launcher offline`.

## Windows

Скачать и установить [MSY32](https://www.msys2.org/).

Скачать и установить [Java JDK](https://adoptium.net/). При установки не забыть выставить, чтобы в `PATH` прописало.

Запустить "`MSYS MinGW x86`". **Именно x86**. В появившемся терминале воодим команды:

``` bash
> pacman -Syu pactoys git
> pacboy -S toolchain:p cmake:p ninja:p qt6-base:p qt6-5compat:p qt6-svg:p qt6-imageformats:p quazip-qt6:p extra-cmake-modules:p
> pacman -S patch

> git clone --recursive https://github.com/PrismLauncher/PrismLauncher.git
> cd PrismLauncher
> wget https://gitlab.com/The220th/prism-launcher-offline/-/raw/main/0001-Remove-DRM.patch -O 0001-Remove-DRM.patch
> patch -p1 < "0001-Remove-DRM.patch"

> cmake -Bbuild -DCMAKE_INSTALL_PREFIX=install -DENABLE_LTO=ON -DLauncher_QT_VERSION_MAJOR=6
> cmake --build build -j{X} # {X} - кол-во ядер на проце. Например "cmake --build build -j4"
> cmake --install build
> cmake --install build --component portable
> cp /mingw32/bin/libcrypto-1_1.dll /mingw32/bin/libssl-1_1.dll install
```

В папке `./install` появится портативный `Prism Launcher offline`.
